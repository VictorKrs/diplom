#include <QtConcurrent/QtConcurrent>

#include "mainwindow.h"
#include "ui_mainwindow.h"
//#include <QErrorMessage>
//#include <QFileDialog>
//#include <QPainter>
//#include <fstream>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    hookRun = false;

    ui->setupUi(this);

    ui->pushButtonRunStop->setVisible(false);
    ui->lineEditIP->setText("192.168.0.101");
    ui->lineEditPort->setInputMask("0000");
    ui->lineEditPort->setText("6666");
    ui->lineEditKey->setText("/dev/input/event4");
    ui->lineEditMouse->setText("/dev/input/mice");

    connect(ui->pushButtonConnect, &QPushButton::clicked, this, &MainWindow::onButtonConnectClicked);
    connect(ui->pushButtonRunStop, &QPushButton::clicked, this, &MainWindow::onButtonRunStopClicked);

    connect(this, &MainWindow::connectHook, &hook, &hookSpace::THook::onConnectionRequest);
    connect(this, &MainWindow::runHook, &hook, &hookSpace::THook::runHook);
    connect(this, &MainWindow::stopHook, &hook, &hookSpace::THook::stopHook);
    connect(this, &MainWindow::changeFileKeyName, &hook, &hookSpace::THook::onChangeFileKeyName);
    connect(this, &MainWindow::changeFileMouseName, &hook, &hookSpace::THook::onChangeFileMouseName);

    connect(&hook, &hookSpace::THook::connectionDone, this, &MainWindow::onConnectionDone);
    connect(&hook, &hookSpace::THook::socketDisconnected, this, &MainWindow::onDisconnected);
    connect(&hook, &hookSpace::THook::hookStarted, this, &MainWindow::onHookStarted);
    connect(&hook, &hookSpace::THook::hookStopped, this, &MainWindow::onHookStopped);
    connect(&hook, &hookSpace::THook::error, this, &MainWindow::onHookError);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::onConnectionDone(){
    messager.showMessage("Соединение установлено");

    emit changeFileKeyName(ui->lineEditKey->text());
    emit changeFileMouseName(ui->lineEditMouse->text());

    connectionChanged(true);
}

void MainWindow::onDisconnected(){
    messager.showMessage("Соединение разорвано");

    connectionChanged(false);
}

void MainWindow::connectionChanged(bool isConnected){
    ui->pushButtonRunStop->setVisible(isConnected);
    ui->pushButtonConnect->setVisible(!isConnected);
    ui->lineEditIP->setEnabled(!isConnected);
    ui->lineEditKey->setEnabled(!isConnected);
    ui->lineEditMouse->setEnabled(!isConnected);
    ui->lineEditPort->setEnabled(!isConnected);
}

void MainWindow::onHookStarted(){
    hookRun = true;
    ui->pushButtonRunStop->setText("Stop hook");
}

void MainWindow::onHookError(hookSpace::hookErrors error){
    if(error == hookSpace::connectionFailed)
        messager.showMessage("Ошибка подключения к редактору");
    if(error == hookSpace::noConnection)
        messager.showMessage("Нет соединения с редактором");
    if(error == hookSpace::hookIsRunned){
        messager.showMessage("Захватчик уже запущен");
        onHookStarted();
    }
    if(error == hookSpace::hookIsStopped){
        messager.showMessage("Захватчик уже остановлен");
        onHookStopped();
    }
    if(error == hookSpace::errorStart)
        messager.showMessage("Не удалось запустить процесс перехвата");
    if(error == hookSpace::convertError)
        messager.showMessage("Ошибка преобразования события");
}

void MainWindow::onHookStopped(){
    hookRun = false;
    ui->pushButtonRunStop->setText("Run hook");
}

void MainWindow::onButtonConnectClicked(){
    emit connectHook(ui->lineEditIP->text(), ui->lineEditPort->text().toInt());
}

void MainWindow::onButtonRunStopClicked(){
    if(hookRun)
        emit stopHook();
    else
        emit runHook();
}
