#ifndef TEVENT_H
#define TEVENT_H

#include <QList>
#include <QPoint>
#include <QXmlStreamWriter>

namespace hookSpace {
    class TModifier;
    class TArea;
    class TEvent;
    class TKeyPressEvent;
    class TMouseEvent;
    class TEventsConvertor;

    enum ModifierNames{ctrl, shift, alt, win, no};
    enum TypesOfEvents {keyPres, mouseClick, mouseDragStart, mouseDragTrack, mouseDragStop, mouseWheel};
    enum TypesOfMouseButton{Left, Right, Wheel};
    enum Ways{Upward, Downward};

    void writeLog(QString);
}

Q_DECLARE_METATYPE(hookSpace::ModifierNames)

class hookSpace::TModifier : public QObject{
Q_OBJECT
public:
    TModifier();
    TModifier(const TModifier &m);

    QString toQString();

public slots:
   void clearAll();
   void removeModifier(ModifierNames m);
   void addModifier(ModifierNames m);
   void setModifiers(TEvent* ev);

private:
    int ctrlM, shiftM, altM, winM;
};

class hookSpace::TArea {
public:
    TArea(int x1, int y1, int width1, int height1);

    int x, y, width, height;
};

class hookSpace::TEvent{
public:
    TEvent();
    virtual ~TEvent();

    QString* convert();
    void setImageName(QString* imName);
    bool haveImageName();
    void setModifiers(TModifier* m);

protected:
    bool loadImage();
    virtual void convertSpecific() = 0;

    hookSpace::TModifier* modifiers;
    QString type;   // keyPress / mouseWheel / mouseClick / mouseDragStart / mouseDragTrack / mouseDragStop
    QString *imageName;
    QByteArray *image;
    QXmlStreamWriter *xmlWriter;
};

class hookSpace::TKeyPressEvent : public hookSpace::TEvent{
public:
    TKeyPressEvent();
    TKeyPressEvent(QString k);

    void setKey(QString k);

private:
    void convertSpecific();

    QString key;
};

class hookSpace::TMouseEvent : public hookSpace::TEvent{
public:
    TMouseEvent(TypesOfEvents typeOfDrag);
    TMouseEvent(TypesOfMouseButton t);
    TMouseEvent(Ways w);

    ~TMouseEvent();

    void setAsDragStart();
    void addClick();
    void setPoint(QPoint p);
    int getCountClick();
    hookSpace::TypesOfMouseButton getTypeButton();
    hookSpace::TypesOfEvents getTypeEvent();

private:
    void convertSpecific();

    hookSpace::TypesOfEvents typeEvent;
    hookSpace::TypesOfMouseButton typeButton;
    QString mouseButton;
    QPoint point;
    QString scroll;    // Upward / Downward
    hookSpace::TArea *window;
    int countClick;
};

class hookSpace::TEventsConvertor : public QObject{
Q_OBJECT
public slots:
    void onConvertEvent(TEvent* event);

signals:
    void convertReady(QString* xmlEvent);
    void convertError();
};
#endif // TEVENT_H
