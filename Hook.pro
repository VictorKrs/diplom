#-------------------------------------------------
#
# Project created by QtCreator 2017-02-14T13:51:52
#
#-------------------------------------------------

QT       += core gui
QT += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Hook
LIBS	+= -L/usr/include -lX11
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    Hook.cpp \
    Event.cpp \
    Client.cpp \
    DeviceHook.cpp

HEADERS  += mainwindow.h \
    Hook.h \
    Event.h \
    Client.h \
    DeviceHook.h

FORMS    += mainwindow.ui

OTHER_FILES += \
    KeyCodes.txt \
    Problems.txt \
    Heap.txt \
    UsefulLinks.txt \
    HookOld.txt \
    About.txt
