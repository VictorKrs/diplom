#include <QTime>
#include <QFile>
#include <QTextStream>

#include "Event.h"

hookSpace::TEvent::TEvent(){
    imageName = 0, xmlWriter = 0;
    modifiers = 0, image = 0;
}

hookSpace::TEvent::~TEvent(){
    QFile im(*imageName);
    if(im.exists())
        im.remove();
    delete imageName;
    delete xmlWriter;
    delete modifiers;
    delete image;
}

void hookSpace::TEvent::setImageName(QString* imName){
    imageName = imName;
}

void hookSpace::TEvent::setModifiers(TModifier* m){
    modifiers = m;
}

bool hookSpace::TEvent::haveImageName(){
    return imageName != 0;
}

bool hookSpace::TEvent::loadImage(){
    while(!imageName) {}
    QFile im(*imageName);
    while(!im.exists()) {}
    if(!im.open(QIODevice::ReadOnly))
        return 0;

    image = new QByteArray(im.readAll());

    im.remove();
    im.close();

    return 1;
}

QString* hookSpace::TEvent::convert(){
    QTime time = QTime::currentTime();
    long long msTime = (((long long)(time.hour()) * 60 + time.minute()) * 60 + time.second()) * 1000 + time.msec();

    while (!modifiers) { }
    QString mod = modifiers->toQString();

    QString *eventXml = new QString();
    xmlWriter = new QXmlStreamWriter(eventXml);

    if(!loadImage())
        return 0;

    xmlWriter->writeStartElement("content.snapshot");
    xmlWriter->writeAttribute("type", type);
    xmlWriter->writeAttribute("time", QString::number(msTime));

    xmlWriter->writeStartElement("picture");
    xmlWriter->writeAttribute("size", QString::number(image->length()));
    xmlWriter->writeCharacters(image->toBase64());
    xmlWriter->writeEndElement();

    if(!mod.isEmpty()){
        xmlWriter->writeStartElement("mod");
        xmlWriter->writeCharacters(mod);
        xmlWriter->writeEndElement();
    }

    this->convertSpecific();

    xmlWriter->writeEndElement();   // content.snapshot

    if(xmlWriter->hasError())
        return 0;

    return eventXml;
}

hookSpace::TKeyPressEvent::TKeyPressEvent(){
    type = "keyPress";
}

hookSpace::TKeyPressEvent::TKeyPressEvent(QString k){
    type = "keyPress";
    key = k;
}

void hookSpace::TKeyPressEvent::setKey(QString k){
    key = k;
}

void hookSpace::TKeyPressEvent::convertSpecific(){
    if (!xmlWriter)
        return;

    xmlWriter->writeStartElement("event");
    xmlWriter->writeAttribute("key", key);
    xmlWriter->writeEndElement();
}

hookSpace::TMouseEvent::TMouseEvent(TypesOfEvents typeOfDrag){
// По факту, этот конструктор не используется для начала перенаскивания,
// но может кто-то что-нибудь придумает
    typeEvent = typeOfDrag;
    countClick = 0;
    window = 0;
    typeButton = Left;

    switch (typeOfDrag) {
    case mouseDragStart:
        type = "mouseDragStart";
        break;
    case mouseDragTrack:
        type = "mouseDragTrack";
        break;
    case mouseDragStop:
        type = "mouseDragStop";
        break;
    default:
    break;
    }
}

hookSpace::TMouseEvent::TMouseEvent(TypesOfMouseButton t){
    window = new TArea(point.x(), point.y(), 500, 500);
    type = "mouseClick";
    countClick = 0;
    typeEvent = mouseClick;
    typeButton = t;
    mouseButton = (t == Left) ? "Left" : "Right";
}

hookSpace::TMouseEvent::TMouseEvent(Ways w){
    type = "mouseWheel";
    typeButton = Wheel;
    typeEvent = mouseWheel;
    if (w == Upward)
        scroll = "Upward";
    if (w == Downward)
        scroll = "Downward";
    window = 0;
    countClick = 0;
}

hookSpace::TMouseEvent::~TMouseEvent(){
    delete window;
}

void hookSpace::TMouseEvent::addClick(){
    if(countClick < 2 && typeEvent == mouseClick)
        countClick++;
}

int hookSpace::TMouseEvent::getCountClick(){
    return countClick;
}

hookSpace::TypesOfMouseButton hookSpace::TMouseEvent::getTypeButton(){
    return typeButton;
}

hookSpace::TypesOfEvents hookSpace::TMouseEvent::getTypeEvent(){
    return typeEvent;
}

void hookSpace::TMouseEvent::setAsDragStart(){
    typeEvent = mouseDragStart;
    type = "mouseDragStart";
}

void hookSpace::TMouseEvent::setPoint(QPoint p){
    point = p;
}

void hookSpace::TMouseEvent::convertSpecific(){
    if (!xmlWriter)
        return;

    if(typeEvent == mouseClick || typeEvent == mouseDragStart || typeEvent == mouseWheel)
        xmlWriter->writeStartElement("event");
    if(typeEvent == mouseClick || typeEvent == mouseDragStart)
        xmlWriter->writeAttribute("button", mouseButton);
    if(typeEvent == mouseClick)
        xmlWriter->writeAttribute("count", QString::number(countClick));
    if(typeEvent == mouseWheel)
        xmlWriter->writeAttribute("way", scroll);
    if(typeEvent != mouseWheel){
        xmlWriter->writeStartElement("point");
        xmlWriter->writeAttribute("x", QString::number(point.x()));
        xmlWriter->writeAttribute("y", QString::number(point.y()));
        xmlWriter->writeEndElement();
    }
    if(typeEvent == mouseClick){
        xmlWriter->writeStartElement("window");
        xmlWriter->writeAttribute("x", QString::number(0));
        xmlWriter->writeAttribute("y", QString::number(0));
        xmlWriter->writeAttribute("width", QString::number(500));
        xmlWriter->writeAttribute("height", QString::number(500));
        xmlWriter->writeEndElement();
    }
    if(typeEvent == mouseClick || typeEvent == mouseDragStart || typeEvent == mouseWheel)
        xmlWriter->writeEndElement(); // event
}

void hookSpace::TEventsConvertor::onConvertEvent(TEvent* event){
    QString *res = event->convert();

    if (res)
        emit convertReady(res);
    else
        emit convertError();
    delete event;
}

hookSpace::TModifier::TModifier(){    
    qRegisterMetaType<hookSpace::ModifierNames>("ModifierNames");
    altM = 0, ctrlM = 0, shiftM = 0, winM = 0;
}

hookSpace::TModifier::TModifier(const TModifier &m)
    :QObject::QObject()
{
    altM = m.altM, ctrlM = m.ctrlM, shiftM = m.shiftM, winM = m.winM;
}

void hookSpace::TModifier::addModifier(ModifierNames m){
    if (m == alt) altM = 1;
    if (m == ctrl) ctrlM = 1;
    if (m == shift) shiftM = 1;
    if (m == win) winM = 1;
}

void hookSpace::TModifier::removeModifier(ModifierNames m){
    if (m == alt) altM = 0;
    if (m == ctrl) ctrlM = 0;
    if (m == shift) shiftM = 0;
    if (m == win) winM = 0;
}

void hookSpace::TModifier::setModifiers(TEvent* ev){
    ev->setModifiers(new TModifier(*this));
}

QString hookSpace::TModifier::toQString(){
    QString res;

    if (altM)
        res += "Alt";
    if (ctrlM){
        if(res.length()) res += ", ";
        res += "Control";
    }
    if (shiftM){
        if(res.length()) res += ", ";
        res += "Shift";
    }
    if (winM){
        if(res.length()) res += ", ";
        res += "Windows";
    }

    return res;
}

void hookSpace::TModifier::clearAll(){
    altM = 0, ctrlM = 0, shiftM = 0, winM = 0;
}

hookSpace::TArea::TArea(int x1, int y1, int width1, int height1){
    x = x1, y = y1, width = width1, height = height1;
}

void hookSpace::writeLog(QString s){
    QFile log("log.txt");
    if(!log.open(QIODevice::Append))
        return;
    QTextStream stream(&log);
    stream << s + '\n';
    log.close();
}
