#ifndef THOOK_H
#define THOOK_H

#include <QString>
#include <QTime>
#include <QThread>

//#include "Event.h"
#include "Client.h"
#include "DeviceHook.h"

namespace hookSpace {
    class THook;
    enum hookErrors {connectionFailed, noConnection, hookIsRunned, hookIsStopped, errorStart, convertError};
}

class hookSpace::THook : public QObject{
Q_OBJECT
// Своего рода диспетчер, обеспечивает взаимодействие между всеми объектами
public:
    THook();
    ~THook();

public slots:
    void onConnectionRequest(QString ip, int port);
    void onSocketConnected();
    void onDisconnected();
    void onErrorConnected();

    void runHook();
    void stopHook();

    void onHookDeviceRunned();
    void onHookDeviceStopped();
    void onHookDeviceError();
    void onChangeFileKeyName(QString name);
    void onChangeFileMouseName(QString name);
    void onEventReady(TEvent* event);

    void onConvertionDone(QString* event);
    void onConvertionError();

signals:
// сигналы клиенту
    void requestConnection(const QString ip, const int port);
    void sendData(QString* data);

// сигналы mainwindow
    void connectionDone();
    void socketDisconnected();
    void hookStarted();
    void hookStopped();

// сигналы перехватчикам
    void run();
    void stop();
    void setFileKeyName(const QString name);
    void setFileMouseName(const QString name);

// сигналы конвертору
    void convertionRequest(TEvent* event);

// сигнал об ошибке
    void error(hookErrors err);

private:
    void checkState();

    QThread threadConvertor, threadClient, threadHookDevices;
    TModifier modifier;
    bool isRun;
    int countActiveHookDevice, countHookDeviceError;
    int countDevice;
};

#endif // THOOK_H
