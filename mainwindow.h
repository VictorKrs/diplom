#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
//#include <QSystemTrayIcon>
#include <QErrorMessage>

#include "Hook.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();


public slots:
    void onConnectionDone();
    void onDisconnected();

    void onHookStarted();
    void onHookStopped();
    void onHookError(hookSpace::hookErrors error);

    void onButtonConnectClicked();
    void onButtonRunStopClicked();

signals:
    void connectHook(QString ip, int port);
    void runHook();
    void stopHook();
    void changeFileKeyName(QString name);
    void changeFileMouseName(QString name);

private:
    void connectionChanged(bool isConnected);

    Ui::MainWindow *ui;
    //QSystemTrayIcon *ic;
    QErrorMessage messager;
    hookSpace::THook hook;
    bool hookRun;
};


#endif // MAINWINDOW_H
