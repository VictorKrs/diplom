#include <QtConcurrent/QtConcurrent>
#include <QFile>
#include <QCursor>

#include <stdlib.h>
#include <fstream>
#include <string.h>
#include <math.h>

#include "DeviceHook.h"

hookSpace::THookDevice::THookDevice(TModifier *modif){
    modifier = modif;
    event = 0, isRun = false, countScreen = 0;
    eventIsNew = true;

    connect(this, &hookSpace::THookDevice::setModifiers, modifier, &hookSpace::TModifier::setModifiers);
    connect(this, &hookSpace::THookDevice::getScreen, this, &hookSpace::THookDevice::onGetScreen);
}

hookSpace::THookDevice::~THookDevice(){
    if(driverFile) std::fclose(driverFile);
}

void hookSpace::THookDevice::setFileName(const QString name){
    strcpy(fileName, name.toLocal8Bit().data());
}

void hookSpace::THookDevice::onHookRun(){
    if(isRun){
        emit hookDeviceRunned();
        return;
    }

    driverFile = std::fopen(fileName, "r");
    if(driverFile){
        isRun = true;
        QtConcurrent::run(this, &hookSpace::THookDevice::run);
        emit hookDeviceRunned();
    }
    else
        emit hookDeviceError();
}

void hookSpace::THookDevice::onHookStop(){
    if(isRun){
        isRun = false;
        emit hookDeviceStopped();
    }
}

void hookSpace::THookDevice::run(){
    FILE *file = driverFile;

    while(isRun && file == driverFile)
        this->getEvent();

    std::fclose(file);
}

void hookSpace::THookDevice::onGetScreen(hookSpace::TEvent* event){
// К сожалению, пока так. Но и это вполне эффективно
    QString *name = new QString("./Images/" + this->generateImageName() + ".png");
    QString comand = "scrot " + *name;
    system(comand.toLatin1().data());
    event->setImageName(name);
}

void hookSpace::THookDevice::sendEvent(){
    emit changeStatusEvent(false);
    emit setModifiers(event);
    emit eventReady(event);
    event = 0;
}

hookSpace::THookMouse::THookMouse(TModifier *modif)
    : hookSpace::THookDevice::THookDevice(modif)
{
    timerRun = false, mouseDown = false;
    dragRun = false;
}

void hookSpace::THookMouse::getEvent(){
    char buf[3];
    FILE *fileMouse = driverFile;
    if (std::fread(buf, 1, 3, fileMouse) && isRun && fileMouse == driverFile)
        signalProcessing(buf[0], buf[1], buf[2]);
}

QString hookSpace::THookMouse::generateImageName(){
    return "MouseEvent" + QString::number(countScreen++);
}

hookSpace::TMouseEvent* hookSpace::THookMouse::createNewDragEvent(TypesOfEvents typeDrag){
    TMouseEvent *  ev = new TMouseEvent(typeDrag);
    emit getScreen(ev);
    emit setModifiers(ev);
    emit changeStatusEvent(true);
    return ev;
}

hookSpace::TMouseEvent* hookSpace::THookMouse::createNewClickEvent(TypesOfMouseButton typeButton){
    TMouseEvent *ev = new TMouseEvent(typeButton);
    emit getScreen(ev);
    emit setModifiers(ev);
    emit changeStatusEvent(true);
    return ev;
}

hookSpace::TMouseEvent* hookSpace::THookMouse::createNewScrollEvent(Ways way){
    TMouseEvent *ev = new TMouseEvent(way);
    emit getScreen(ev);
    emit setModifiers(ev);
    emit changeStatusEvent(true);
    return ev;
}

void hookSpace::THookMouse::runTimer(TEvent *event1){
    while(timerRun && dynamic_cast<hookSpace::TMouseEvent*>(event1)->getCountClick() != 2 && timeOfClick.msecsTo(QTime::currentTime()) < 500)  { }
    // пол секунды на то, чтобы сделать второй клик, значение практически выдумано

    timerRun = false;
    emit changeStatusEvent(false);
    emit eventReady(event1);
}

void hookSpace::THookMouse::signalProcessing(int x0, int x1, int x2){
// Обрабатывает события мыши
// На вход 3 числа: x0 - код события, x1 и x2 - движение курсора по координатам x и y
// x0 = { 8 - отпустили; 9 - ЛКМ; 10 - ПКМ }
// P.S. Сигнал 8 0 0 приходит в трёх случаях: 1) кнопку мыши отпустили; 2) прокрутили колёсико вверх; 3) прокрутка колёсика вниз
// Из-за этого точно индентефецировать направление прокрутки колёсика мыши данным способом невозможно
// Сохранение события клика кнопки мыши происходит в ф-ии runTimer

    if (mouseDown && (x1 || x2)) {
    // Перетаскивание
        QPoint currentPos = QCursor::pos();
        if (sqrt(pow(currentPos.x() - lastCursorPos.x(), 2) + pow(currentPos.y() - lastCursorPos.y(), 2)) > 24){
            TMouseEvent *currentEvent = createNewDragEvent(mouseDragTrack);

            if (!dragRun) {
                dragRun = true;
                dynamic_cast<hookSpace::TMouseEvent*>(event)->setAsDragStart();
                sendEvent();
            }

            currentEvent->setPoint(currentPos);
            event = currentEvent;
            sendEvent();
            lastCursorPos = currentPos;
        }
    }

    if (!mouseDown && (x0 == 9 || x0 == 10)){
    // Нажатие кнопки мыши
        TypesOfMouseButton type = (x0 == 9) ? Left : Right;
        if (!event || !timerRun || x0 == 10)
            event = createNewClickEvent(type);

        QPoint currentPos = QCursor::pos();
        if (timerRun && dynamic_cast<hookSpace::TMouseEvent*>(event)->getCountClick())
            if (abs(currentPos.x() - lastCursorPos.x()) > 10 || abs(currentPos.y() - lastCursorPos.y()) > 10) {
            // если условие НЕ выполняется, то это двойной клик ЛКМ,
            // а иначе, пользователь слишком шустро тыкает в разные области экрана
            // значение 10-ти пикселей было взято из головы, возможно, оно не оптимально (погрешность, мало ли руки дрожат)
                timerRun = false;
                event = createNewClickEvent(type);
            }

        lastCursorPos = currentPos, mouseDown = true;
        dynamic_cast<hookSpace::TMouseEvent*>(event)->addClick();

        if (dynamic_cast<hookSpace::TMouseEvent*>(event)->getCountClick() == 1) {
            dynamic_cast<hookSpace::TMouseEvent*>(event)->setPoint(currentPos);
            if (type == Left) { timeOfClick = QTime::currentTime(); }
        }
        if (dynamic_cast<hookSpace::TMouseEvent*>(event)->getCountClick() == 2)
            event = 0;
    }

    if (!mouseDown && x0 == 8 && !(x1 || x2)){
    // Движение колёсика
        event = createNewScrollEvent(Upward);
        sendEvent();
    }

    if (mouseDown && x0 == 8 && !(x1 || x2)) {
    // Отпускание кнопки мыши
        TMouseEvent *currentEvent = dynamic_cast<hookSpace::TMouseEvent*>(event);
        mouseDown = false;
        if(event && currentEvent->getTypeEvent() == mouseClick
                && currentEvent->getTypeButton() == Left
                && currentEvent->getCountClick() == 1)
        {
            timerRun = true;
            QtConcurrent::run(this, &hookSpace::THookMouse::runTimer, event);
        }

        if(event && currentEvent->getTypeEvent() == mouseClick &&
                currentEvent->getTypeButton() == Right)
            sendEvent();

        if(!event && dragRun){
            event = createNewDragEvent(mouseDragStop);
            dynamic_cast<hookSpace::TMouseEvent*>(event)->setPoint(QCursor::pos());
            dragRun = false;
            sendEvent();
        }
        if(dragRun){
            dragRun = false;
            emit hookDeviceError();
        }
    }
}

hookSpace::THookKeyboard::THookKeyboard(TModifier *modif)
    : hookSpace::THookDevice::THookDevice(modif)
{
    connect(this, &hookSpace::THookKeyboard::addModifier, modifier, &hookSpace::TModifier::addModifier);
    connect(this, &hookSpace::THookKeyboard::removeModifier, modifier, &hookSpace::TModifier::removeModifier);
}

void hookSpace::THookKeyboard::onChangeStatusEvent(bool isNew){
// Если данный слот связан с сигналами changeStausEvent,
// то изменение статуса происходит в потоке, в котором создан объект данного класса
    eventIsNew = isNew;
}


void hookSpace::THookKeyboard::getEvent(){
    char buf[24];
    FILE *fileKeyboard = driverFile;
    if(fread(buf, 1, 24, fileKeyboard) && isRun && fileKeyboard == driverFile && buf[16] == 1 && buf[17] == 0 && buf[19] == 0 && buf[20] != 2)
        signalProcessing(buf[18], buf[20]);
}

QString hookSpace::THookKeyboard::generateImageName(){
    return "KeyBoardEvent" + QString::number(countScreen++);
}

hookSpace::TKeyPressEvent* hookSpace::THookKeyboard::createNewKeyPressEvent(){
    TKeyPressEvent* ev = new TKeyPressEvent();
    emit getScreen(ev);
    emit changeStatusEvent(true);
    return ev;
}

hookSpace::ModifierNames hookSpace::THookKeyboard::modifierName(int keyCode){
    ModifierNames res = no;
    if (keyCode == 29 || keyCode == 97){ res = ctrl; }
    if (keyCode == 42 || keyCode == 54){ res = shift; }
    if (keyCode == 56 || keyCode == 100){  res = alt; }
    if (keyCode == 125){ res = win; }
    return res;
}

QString hookSpace::THookKeyboard::searchKey(int keyCode){
    QFile KeyFile("./KeyCodes.txt");
    if(!KeyFile.open(QIODevice::ReadOnly | QIODevice::Text))
        return "";

    QTextStream in(&KeyFile);
    for (int i = 1; i < keyCode; in.readLine(), i++) { }
    QString res = in.readLine();

    KeyFile.close();
    return res;
}

void hookSpace::THookKeyboard::signalProcessing(int keyCode, int eventCode){
// обрабатывает событие клавиатуры
// eventCode - тип события (0 - отпустили клавишу, 1 - нажали, 2 - зажата)
    ModifierNames modifierKey = modifierName(keyCode);
    if(eventCode == 0){
        if(modifierKey != no){
            if (eventIsNew && event){
                dynamic_cast<hookSpace::TKeyPressEvent*>(event)->setKey(searchKey(keyCode));
                emit removeModifier(modifierKey);
                sendEvent();
            }
            else{
                emit removeModifier(modifierKey);
 //               delete event;
            }
        }
    }

    if(eventCode == 1){
        if (!event){
            event = createNewKeyPressEvent();
        }
        if (modifierKey != no){
            emit addModifier(modifierKey);}
        else{
            dynamic_cast<hookSpace::TKeyPressEvent*>(event)->setKey(searchKey(keyCode));
            sendEvent();
        }
    }
}

void hookSpace::getScreenCrutch(THookDevice *hookDev, TEvent* ev){
    hookDev->getScreen(ev);
}
