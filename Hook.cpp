#include <QFile>
#include <QCursor>

#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "Hook.h"

hookSpace::THook::THook(){
    isRun = false;
    countActiveHookDevice = 0;
    countHookDeviceError = 0;
    countDevice = 2;

    TClient* client = new TClient;    
    client->moveToThread(&threadClient);
    threadClient.start(QThread::TimeCriticalPriority);

    TEventsConvertor *convertor = new TEventsConvertor;
    convertor->moveToThread(&threadConvertor);
    //threadConvertor.start(QThread::LowestPriority);
    threadConvertor.start();

    THookKeyboard* hookKeyboard = new THookKeyboard(&modifier);
    THookMouse* hookMouse = new THookMouse(&modifier);
    hookKeyboard->moveToThread(&threadHookDevices);
    hookMouse->moveToThread(&threadHookDevices);
    //threadHookDevices.start(QThread::TimeCriticalPriority);
    threadHookDevices.start();

// Связывание сигналов и слотов
// Все объекты общаются между собой не через вызов функций,
// А через механизм сигналов и слотов
// Функция слота объекта выполняется в потоке, в котором находится этот объект
    connect(this, &hookSpace::THook::requestConnection, client, &hookSpace::TClient::onConnectionRequest);
    connect(this, &hookSpace::THook::sendData, client, &hookSpace::TClient::onSendMessage);
    connect(this, &hookSpace::THook::run, client, &hookSpace::TClient::onStarted);
    connect(this, &hookSpace::THook::stop, client, &hookSpace::TClient::onStopped);
    connect(client, &hookSpace::TClient::socketConnected, this, &hookSpace::THook::onSocketConnected);
    connect(client, &hookSpace::TClient::errorConnected, this, &hookSpace::THook::onErrorConnected);
    connect(client, &hookSpace::TClient::socketDisconnected, this, &hookSpace::THook::onDisconnected);

    connect(this, &hookSpace::THook::convertionRequest, convertor, &hookSpace::TEventsConvertor::onConvertEvent);
    connect(convertor, &hookSpace::TEventsConvertor::convertReady, this, &hookSpace::THook::onConvertionDone);
    connect(convertor, &hookSpace::TEventsConvertor::convertError, this, &hookSpace::THook::onConvertionError);

    connect(this, &hookSpace::THook::run, hookKeyboard, &hookSpace::THookKeyboard::onHookRun);
    connect(this, &hookSpace::THook::stop, hookKeyboard, &hookSpace::THookKeyboard::onHookStop);
    connect(this, &hookSpace::THook::setFileKeyName, hookKeyboard, &hookSpace::THookKeyboard::setFileName);
    connect(hookKeyboard, &hookSpace::THookKeyboard::hookDeviceRunned, this, &hookSpace::THook::onHookDeviceRunned);
    connect(hookKeyboard, &hookSpace::THookKeyboard::hookDeviceStopped, this, &hookSpace::THook::onHookDeviceStopped);
    connect(hookKeyboard, &hookSpace::THookKeyboard::hookDeviceError, this, &hookSpace::THook::onHookDeviceError);
    connect(hookKeyboard, &hookSpace::THookKeyboard::eventReady, this, &hookSpace::THook::onEventReady);

    connect(this, &hookSpace::THook::run, hookMouse, &hookSpace::THookMouse::onHookRun);
    connect(this, &hookSpace::THook::stop, hookMouse, &hookSpace::THookMouse::onHookStop);
    connect(this, &hookSpace::THook::setFileMouseName, hookMouse, &hookSpace::THookMouse::setFileName);
    connect(hookMouse, &hookSpace::THookMouse::hookDeviceRunned, this, &hookSpace::THook::onHookDeviceRunned);
    connect(hookMouse, &hookSpace::THookMouse::hookDeviceStopped, this, &hookSpace::THook::onHookDeviceStopped);
    connect(hookMouse, &hookSpace::THookMouse::hookDeviceError, this, &hookSpace::THook::onHookDeviceError);
    connect(hookMouse, &hookSpace::THookMouse::eventReady, this, &hookSpace::THook::onEventReady);

    connect(hookMouse, &hookSpace::THookMouse::changeStatusEvent, hookKeyboard, &hookSpace::THookKeyboard::onChangeStatusEvent);
    connect(hookKeyboard, &hookSpace::THookKeyboard::changeStatusEvent, hookKeyboard, &hookSpace::THookKeyboard::onChangeStatusEvent);
}

hookSpace::THook::~THook(){
    if(isRun)
        emit stop();

    threadClient.quit();
    threadConvertor.quit();
    threadHookDevices.quit();
    threadConvertor.wait();
    threadClient.wait();
//    threadHookDevices.wait();
}

void hookSpace::THook::onConnectionRequest(QString ip, int port){
    emit requestConnection(ip, port);
}

void hookSpace::THook::onSocketConnected(){
    emit connectionDone();
}

void hookSpace::THook::onDisconnected(){
    emit socketDisconnected();
    if(isRun)
        emit stop();
}

void hookSpace::THook::onErrorConnected(){
    emit error(connectionFailed);
}

void hookSpace::THook::runHook(){
    if(isRun)
        emit error(hookIsRunned);
    else
        emit run();
}

void hookSpace::THook::stopHook(){
    if(isRun)
        emit stop();
    else
        emit error(hookIsStopped);
}

void hookSpace::THook::checkState(){
    if (countActiveHookDevice == countDevice){
        isRun = true;
        emit hookStarted();
    }
    else
        if(countActiveHookDevice + countHookDeviceError == countDevice){
            emit stop();
            emit error(errorStart);
            countHookDeviceError = 0;
        }
}

void hookSpace::THook::onHookDeviceRunned(){
    countActiveHookDevice++;
    checkState();
}

void hookSpace::THook::onHookDeviceStopped(){
    if(--countActiveHookDevice == 0){
        isRun = false;
        emit hookStopped();
    }
}

void hookSpace::THook::onHookDeviceError(){
    countHookDeviceError++;
    checkState();
}

void hookSpace::THook::onChangeFileKeyName(QString name){
    emit setFileKeyName(name);
}

void hookSpace::THook::onChangeFileMouseName(QString name){
    emit setFileMouseName(name);
}

void hookSpace::THook::onEventReady(TEvent* event){
    emit convertionRequest(event);
}

void hookSpace::THook::onConvertionDone(QString *event){
    if(isRun)
        emit sendData(event);
    else
        delete event;
}

void hookSpace::THook::onConvertionError(){
    emit error(convertError);
}
