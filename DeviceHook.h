#ifndef DEVICEHOOK_H
#define DEVICEHOOK_H

#include <QString>
#include <QTime>

#include "Event.h"

namespace hookSpace {
    class THookDevice;
    class THookMouse;
    class THookKeyboard;

    void getScreenCrutch(THookDevice* hookDev, TEvent* ev);
}

class hookSpace::THookDevice : public QObject{
Q_OBJECT
public:
    THookDevice(TModifier *modif);
    ~THookDevice();


public slots:
    void setFileName(const QString name);
    void onHookRun();
    void onHookStop();
    void onGetScreen(hookSpace::TEvent* event);

signals:
    void hookDeviceError();
    void hookDeviceRunned();
    void hookDeviceStopped();
    void eventReady(TEvent *event);
    void changeStatusEvent(bool isNew);
    void setModifiers(TEvent* ev);
    void getScreen(hookSpace::TEvent* event);

protected:
    void run();
    void sendEvent();

    virtual void getEvent() = 0;
    virtual QString generateImageName() = 0;

    int countScreen;
    char fileName[100];
    FILE *driverFile;
    TEvent *event;
    TModifier *modifier;
    bool isRun;
    bool eventIsNew;
};

class hookSpace::THookMouse : public hookSpace::THookDevice{
Q_OBJECT
public:
    THookMouse(TModifier *modif);

private:
    void signalProcessing(int x1, int x2, int x3);
    void runTimer(TEvent *event);
    TMouseEvent* createNewDragEvent(TypesOfEvents typeDrag);
    TMouseEvent* createNewClickEvent(TypesOfMouseButton typeButton);
    TMouseEvent* createNewScrollEvent(Ways way);

    void getEvent();
    QString generateImageName();

    bool timerRun, mouseDown, dragRun;
    QPoint lastCursorPos;
    QTime timeOfClick;
};

class hookSpace::THookKeyboard : public hookSpace::THookDevice{
Q_OBJECT
public:
    THookKeyboard(TModifier *modif);

public slots:
    void onChangeStatusEvent(bool isNew);

signals:
    void addModifier(ModifierNames m);
    void removeModifier(ModifierNames m);

private:
    void signalProcessing(int keyCode, int eventCode);
    ModifierNames modifierName(int keyCode);
    QString searchKey(int keyCode);
    TKeyPressEvent* createNewKeyPressEvent();

    void getEvent();
    QString generateImageName();
};

#endif // DEVICEHOOK_H
